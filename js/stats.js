var timeWatching;
var popularity; 

function updateStats() {
        timeWatching = localStorage.getItem("secondsSinceWatching");
        timeWatching++;
        localStorage.setItem("secondsSinceWatching", timeWatching);
        
        $.ajax({
            url: 'https://www.youtube.com/live_stats?v=' + localStorage.getItem("videoID"),
            crossOrigin: true,
            type: 'GET',
            dataType: "jsonp",
            xhrFields: { withCredentials: true },
            accept: 'application/json'
        }).done(function (data) {
            popularity = Math.floor((data.count * numComments) / (0.016666 * timeWatching)) / 1000;
            document.getElementById("numWatchers").innerHTML = "Current Viewers -> " + data.count + " &nbsp; &nbsp; &nbsp; &nbsp;Populatrity -> " + popularity;
        }).fail(function (xhr, textStatus, error) {
            var title, message;
            switch (xhr.status) {
                case 403:
                    title = xhr.responseJSON.errorSummary;
                    message = 'Please login to your server before running the test.';
                    break;
                default:
                    title = 'Invalid URL or Cross-Origin Request Blocked';
                    message = 'You must explictly add this site (' + window.location.origin + ') to the list of allowed websites in your server.';
                    break;
            }
        });
}