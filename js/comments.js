var numComments = 0;
var timeLast = "";
var newerComment = true;

//Convert the recieved date information into propper format
function convertTime(time) {
        var months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
        var year = "";
        for(i = 0; i < 4; i++){
                year += time[i];}
        var month = "";
        for(k = 5; k < 7; k++){
                month += time[k];}
        var day = "";
        for(j = 8; j < 10; j++){
                day += time[j];}
        var hour = "";
        for(g = 11; g < 13; g++){
                hour += time[g];}
        var minute = "";
        for(h = 14; h < 16; h++){
                minute += time[h];}
        var second = "";
        for(o = 17; o < 19; o++){
                second += time[o];}
        if(month[0] == "0"){
                month = month[1];
        }
        return (months[month-1]+" "+ day +", "+ year +" @ "+ hour+":"+minute+":"+second+" UTC");
}

//This will determine whether to add the comment to the end of the list or the start
function timeGreaterThan(time) {
        if(timeLast == "") return; //Skip the first comment
        
        //Check newer year
        var oldYear = timeLast.substr(0,4);
        var currentYear = time.substr(0,4);
        newerComment = (currentYear > oldYear) ? true:false;
        if(newerComment) return;
        
        //Check newer month
        var oldMonth = timeLast.substr(5,2);
        var currentMonth = time.substr(5,2);     
        newerComment = (currentMonth > oldMonth) ? true:false;
        if(newerComment) return;
       
        //Check newer day
        var oldDay = timeLast.substr(8,2);
        var currentDay = time.substr(8,2);
        newerComment = (currentDay > oldDay) ? true:false;
        if(newerComment) return;
       
        //Check newer hour
        var oldHour = timeLast.substr(11,2);
        var currentHour = time.substr(11,2); 
        newerComment = (currentHour > oldHour) ? true:false;
        if(newerComment) return;
       
        //Check newer minute
        var oldMinute = timeLast.substr(14,2);
        var currentMinute = time.substr(14,2);
        newerComment = (currentMinute > oldMinute) ? true:false;
        if(newerComment) return;
        
        //Check newer second
        var oldSecond = timeLast.substr(17,2);
        var currentSecond = time.substr(17,2);
        newerComment = (currentSecond > oldSecond) ? true:false;
        if(newerComment) return;
}

//Format all the data in the correct order and append it to the chat log in the HTML
function displayMsg(channel, msg, time, icon) {
        //If the comment is new, prepend it to the top of the list
        if(newerComment) {
                $("#chatList").prepend("<div class='chatMsg'>"+
                "<img src='"+icon+"' class='profile-icon'/>"+
                "<div class='chat-name'>"+channel+"</div>"+
                "<div class='chat-date'>"+time+"</div>"+
                "<div class='chat-text'>"+msg+"</div></div>");
        } else {
                $("#chatList").append("<div class='chatMsg'>"+
                "<img src='"+icon+"' class='profile-icon'/>"+
                "<div class='chat-name'>"+channel+"</div>"+
                "<div class='chat-date'>"+time+"</div>"+
                "<div class='chat-text'>"+msg+"</div></div>");
        }
}

//Recieve all data from comments
function executeChannelRequest(request, msg, time) {
    request.execute(function(response) {
            timeGreaterThan(time); //Before sending data update the position either newer or later of the comment
            if(newerComment) { timeLast = time };
            
            //Send data to the formatting function
            displayMsg(response.items[0].snippet.title, msg, convertTime(time), response.items[0].snippet.thumbnails.default.url);
    });
}

//Build a request for channel data given channel ID
function getChannelName(channelId, msg, time) {
        buildApiChannelRequest(msg, time, 'GET',
        '/youtube/v3/channels', {
            'id': channelId,
            'part': 'snippet'
        });
}

//Grab the comments given the live chat ID
function grabMessages(chatID) {
        localStorage.setItem("chatIDLive", chatID); //save the chat id locally
        buildApiMessageRequest('GET',
        '/youtube/v3/liveChat/messages', {
            'liveChatId': chatID,
            'part': 'snippet',
            'pageToken' : localStorage.getItem('commentToken')
        });
}


//Function will grab comments based on username
function LookupComments() {
        document.getElementById("chatList").innerHTML = ""; //clear the buffer
        var name = document.getElementById("getUsername").value; //Get data from the input field
        document.getElementById("getUsername").value = ""; //clear the input field;
        console.log("searching for user: " + name);
        for(i = 0; i < numComments; i++) {
        }
}

//Reset all comment data when switching videos
function ResetComments() {
        localStorage.setItem('commentToken', ''); //reset the token used to load comments 
        for(i = 0; i < numComments; i++) {
                localStorage.removeItem("msg_" + i);
        }
        numComments = 0;
        document.getElementById("chatList").innerHTML = ""; //Clear the chat log from the curent video and then update with the new
}

//Check if the video has already been loaded into the buffer
function ListContain(item) {
        for(i = 0; i < numComments; i++) {
                if(item == localStorage.getItem("msg_" + i)) {
                        return true;
                }
        }
        return false;
}

//Recive back messages
function executeMessageRequest(request) {
    try { request.execute(function(response) {
                    localStorage.setItem('commentToken', response.nextPageToken);
            //Do for all messages
            for(i = 0; i < response.pageInfo.totalResults; i++) {
                    if(!ListContain(response.items[i].id)) {
                            localStorage.setItem('msg_' + numComments, response.items[i].id);
                            //Need to get channel name now from the channel ID;
                            numComments++;
                            getChannelName(response.items[i].snippet.authorChannelId, response.items[i].snippet.displayMessage, response.items[i].snippet.publishedAt);
                    }
            }
    }); }
    catch(err){
            console.log(err);
    }
}

//Recieving live streaming comments data
function executeCommentRequest(request) {
    request.execute(function(response) {
            grabMessages(response.items[0].liveStreamingDetails.activeLiveChatId); //Grab the message array from the live chat
    });
}

//Create the JSON for the request
function buildApiChannelRequest(msg, time, requestMethod, path, params, properties) {
    params = removeEmptyParams(params);
    var request;
    if (properties) {
      var resource = createResource(properties);
      request = gapi.client.request({
          'body': resource,
          'method': requestMethod,
          'path': path,
          'params': params
      });
    } else {
      request = gapi.client.request({
          'method': requestMethod,
          'path': path,
          'params': params
      });
    }
    executeChannelRequest(request, msg, time); //Send the Request + all other data gathered
}

//Create JSON reuest for messages given live chat ID
function buildApiMessageRequest(requestMethod, path, params, properties) {
    params = removeEmptyParams(params);
    var request;
    if (properties) {
      var resource = createResource(properties);
      request = gapi.client.request({
          'body': resource,
          'method': requestMethod,
          'path': path,
          'params': params
      });
    } else {
      request = gapi.client.request({
          'method': requestMethod,
          'path': path,
          'params': params
      });
    }
    executeMessageRequest(request); //Send request
}

//Create the request JSON
function buildApiCommentRequest(requestMethod, path, params, properties) {
    params = removeEmptyParams(params);
    var request;
    if (properties) {
      var resource = createResource(properties);
      request = gapi.client.request({
          'body': resource,
          'method': requestMethod,
          'path': path,
          'params': params
      });
    } else {
      request = gapi.client.request({
          'method': requestMethod,
          'path': path,
          'params': params
      });
    }
    executeCommentRequest(request); //Call the request function on comments details
}

//Load the live streaming details based on video ID
function commentRequest(videoId) {
        buildApiCommentRequest('GET',
        '/youtube/v3/videos', {
            'id': videoId,
            'part': 'snippet,contentDetails,statistics,liveStreamingDetails'
        });
}

//Funclion called on video click
function loadComments() {
        console.log('grabbing comments ' + numComments);
        commentRequest(localStorage.getItem("videoID"));
}