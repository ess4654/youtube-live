function onSuccess(googleUser) {
        console.log('Logged in as: ' + googleUser.getBasicProfile().getName());

        //Add user information to local storage
        localStorage.setItem("signedIn", 1);
        localStorage.setItem("myName", googleUser.getBasicProfile().getName());
        localStorage.setItem("profile-icon", googleUser.getBasicProfile().getImageUrl());
        localStorage.setItem("myId", googleUser.getBasicProfile().getId());
        
        location.reload(); //Refresh the page
}
function onFailure(error) {
        console.log(error);
}

function renderButton() {
        if(localStorage.getItem("signedIn") == 1) {
                console.log('currently signed in');
                document.body.style.backgroundColor = "grey";
                $("#content").load("interface.html"); //Show the website if logged in
                findLiveVideos();
        } else {
                //Render log in button
                gapi.signin2.render('g-signin', {
                'scope': 'profile email',
                'width': 240,
                'height': 50,
                'longtitle': false,
                'theme': 'dark',
                'onsuccess': onSuccess,
                'onfailure': onFailure
                });
        }
}

function LogOut() {
    //delete local data
    localStorage.setItem("signedIn", 0);
    localStorage.removeItem("myName");
    localStorage.removeItem("profile-icon");
    localStorage.removeItem("myId");
    
    //log out of googles api
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      console.log('User signed out.');
    });
    $("#content").load("index.html");
    location.reload(); //Refresh the page
}
