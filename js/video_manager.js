var numVideos = 0;
var pageToken = "";
var timer;
var interval;

// Global variables for GoogleAuth object, auth status.
var GoogleAuth;

/**
 * Load the API's client and auth2 modules.
 * Call the initClient function after the modules load.
 */
function handleClientLoad() {
        gapi.load('client:auth2', initClient);
}

function initClient() {
        // Initialize the gapi.client object, which app uses to make API requests.
        // Get API key and client ID from API Console.
        // 'scope' field specifies space-delimited list of access scopes

        gapi.client.init({
          'clientId': '543515163661-6r9ki7dl8a7nlsttuqb7qe1qgsjldela',
          'discoveryDocs': ['https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest'],
          'scope': 'https://www.googleapis.com/auth/youtube.force-ssl https://www.googleapis.com/auth/youtubepartner'
        }).then(function () {
        GoogleAuth = gapi.auth2.getAuthInstance();

        // Listen for sign-in state changes.
        GoogleAuth.isSignedIn.listen(updateSigninStatus);

        // Handle initial sign-in state. (Determine if user is already signed in.)
        setSigninStatus();
    });
}

function handleAuthClick(event) {
    // Sign user in after click on auth button.
    GoogleAuth.signIn();
}

function setSigninStatus() {
    var user = GoogleAuth.currentUser.get();
    isAuthorized = user.hasGrantedScopes('https://www.googleapis.com/auth/youtube.force-ssl https://www.googleapis.com/auth/youtubepartner');
    // Toggle button text and displayed statement based on current auth status.
    if (isAuthorized) {
      document.getElementById('myIcon').src = localStorage.getItem("profile-icon");
      document.getElementById('messageFont').innerHTML = localStorage.getItem("myName") + ": ";
      videoRequest();
    }
}

function updateSigninStatus(isSignedIn) {
    setSigninStatus();
}

function createResource(properties) {
    var resource = {};
    var normalizedProps = properties;
    for (var p in properties) {
      var value = properties[p];
      if (p && p.substr(-2, 2) == '[]') {
        var adjustedName = p.replace('[]', '');
        if (value) {
          normalizedProps[adjustedName] = value.split(',');
        }
        delete normalizedProps[p];
      }
}
    
for (var p in normalizedProps) {
      // Leave properties that don't have values out of inserted resource.
      if (normalizedProps.hasOwnProperty(p) && normalizedProps[p]) {
        var propArray = p.split('.');
        var ref = resource;
        for (var pa = 0; pa < propArray.length; pa++) {
          var key = propArray[pa];
          if (pa == propArray.length - 1) {
            ref[key] = normalizedProps[p];
          } else {
            ref = ref[key] = ref[key] || {};
          }
        }
      };
    }
    return resource;
}

function removeEmptyParams(params) {
        for (var p in params) {
                if (!params[p] || params[p] == 'undefined') {
                        delete params[p];
                }
        }
        return params;
}

function executeVideoRequest(request) {
        //remove the show more videos button then it will be drawn again later
        if(numVideos > 25) {
                document.getElementById("show-more-videos").parentNode.removeChild(document.getElementById("show-more-videos"));
        }
        
        //get all data, and update the video array
        request.execute(function(response) {
        console.log(response); //Output JSON to the console
        for(i = 0; i < 25; i++) {
                document.getElementById("videoListing").innerHTML += ("<div class='videoList' onClick=updateVideo(&quot;"+
                                                                     response.items[i].id.videoId+
                                                                     "&quot;)><font>" + response.items[i].snippet.title + "</font><img class='thumbnail' src='"+
                                                                     response.items[i].snippet.thumbnails.default.url
                                                                     +"'/></div>"); 
        }
        
        //draw the new button and update the page token
        document.getElementById("videoListing").innerHTML += "<button id='show-more-videos' onClick='findLiveVideos()'>Show More Videos</button>";
        pageToken = response.nextPageToken;
    });
}

function buildApiVideoRequest(requestMethod, path, params, properties) {
        params = removeEmptyParams(params);
        var request;
        if (properties) {
                var resource = createResource(properties);
                request = gapi.client.request({
                'body': resource,
                'method': requestMethod,
                'path': path,
                'params': params
         });
        } else {
                request = gapi.client.request({
                'method': requestMethod,
                'path': path,
                'params': params
        });
    }
    executeVideoRequest(request);
}

//create a query for videos that are livestreams
function videoRequest() {
        // See full sample for buildApiRequest() code, which is not 
        // specific to a particular API or API method.
        console.log('search');
        buildApiVideoRequest('GET',
                '/youtube/v3/search',
                {'eventType': 'live',
                 'maxResults': 25,
                 'pageToken': pageToken,
                 'part': 'snippet',
                 'p': '',
                 'type': 'video'});

}

function findLiveVideos() {
        numVideos += 25;
        handleClientLoad();
}

function updateVideo(videoID) {
        console.log('video changed');
        document.getElementById('video').src = "http://www.youtube.com/embed/" + videoID + "?autoplay=1";
        localStorage.setItem('videoID', videoID);
        ResetComments();
        localStorage.setItem("secondsSinceWatching", 0); //reset the time watching the video
        clearInterval(timer); //remove the timers to prevent infinite looping
        clearInterval(interval); //remove the timers to prevent infinite looping
        timer = setInterval(updateStats, 1000); //Update the stats every 1 second.
        interval = setInterval(loadComments, 1000); //load the live chat for the video, and poll for comments every 100 ms.
}