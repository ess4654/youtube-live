//POST and execute the final response
function executeSendMessageRequest(request) {
        request.execute(function(response) {
        console.log(response);
    });
}

//Build the final request to send message
function buildApiSendMessageRequest(requestMethod, path, params, properties) {
        params = removeEmptyParams(params);
        var request;
        if (properties) {
                var resource = createResource(properties);
                request = gapi.client.request({
                'body': resource,
                'method': requestMethod,
                'path': path,
                'params': params
         });
        } else {
                request = gapi.client.request({
                'method': requestMethod,
                'path': path,
                'params': params
        });
    }
    executeSendMessageRequest(request);
}

//Create message request and send to the builder
function sendMessageRequest(msg) {console.log(localStorage.getItem("chatIDLive"));
        // See full sample for buildApiRequest() code, which is not 
        // specific to a particular API or API method.
        console.log('sending message with data ' + msg + " user: " + localStorage.getItem("myName"));
        console.log(localStorage.getItem("chatIDLive"));
        buildApiSendMessageRequest('POST',
                '/youtube/v3/liveChat/messages',
                {'part' : 'snippet'},
                {'snippet': {
                 'liveChatId' : localStorage.getItem("chatIDLive"),
                 'type' : 'textMessageEvent',
                 "textMessageDetails": {
                      "messageText": msg
                  }}});
}

//Call the message function
function sendMsg() {
        var msg = document.getElementById("message").value;
        document.getElementById("message").value = ""; //Clear the value in the input field
        
        sendMessageRequest(msg);
}